//Program 3 - Game structs and files
// Eric Cardenas - 5/8/2023
// The purpose of this program is to get accustomed to using strcuts and working
// with external data files. Users will enter game titles and related information
// which will then be written to an external data file for viewing later
#include <iostream>
#include <cstring>
#include <fstream> // the new file IO 
#include "games.txt" // placeholder file is created._ UPDATEorFIX

using namespace std;
//Constants
const int SIZE = 26;
const int DESC = 51;

const int QUIT = 6;
//Global placement of struct? 
struct soft{// creates a struct data type w name soft, short for "Software"
	char name[SIZE];// holds the name of game
	char platform[SIZE]; // holds platform name
	char description[DESC]; // Holds the description of said game
	float price = 0.0;
};


// Function prototypes
void menu();
int get_choice();
float capture_price();
void quit();
void save_to();//Saves to file "games.txt"
void load_in();// loads from file 

//MAIN ****************
int main(){// MAIN BODY
	soft games[SIZE]; // declaring the games array struct
	int choice = 0; // User choice
	


//Steps for main
	menu();
	choice = get_choice();// gets choice and catches 
	while(QUIT != choice){//reversed the conditional
	
	}
	quit();




return 0;
}


// Function definitions

//Menu Function. Displays choices
void menu(){
	cout << "\n1. Enter a new game" << endl;
	cout << "2. Display a match of a particular game" << endl;
	cout << "3. Display all games" << endl;
	cout << "4. Save all items to an external data file" << endl;
	cout << "5. Load from an external data file" << endl;
	cout << "6. Quit the program baby!" << endl;


}


//gets choice and returns an int
int get_choice(){
	int num = 0;
	cout << "\nEnter choice: ";
	cin >> num;
	cin.ignore(100, '\n');
return num;
}


//When QUIT is reached
void quit(){
	cout << "You quit the program." << endl;
}
